# BoldLink-PH01

Welcome to my Hello World sample Python app. 

You can run an application with the following instructions:

**1.)** First clone repository
**2.)** Unzip file

_NOTE: You can launch the python app by choosing one of the following two methods:_

1.) Like an executable script. 
  - if you are using Linux 
  You need to make it executable with command: $ **sudo chmod +x BoldLink-PH01.py** )
  - Then, from terminal run it: $ **./BoldLink-PH01.py**

2.) Or with:
   $ **python3 BoldLink-PH01.py**

